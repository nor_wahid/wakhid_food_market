import 'dart:ffi';

import 'package:flutter/material.dart';
import 'ui/pages/pages.dart';

Void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: GeneralPage(
          onBackButtonPressed: () {},
          child: Text("body"),
        ));
  }
}
